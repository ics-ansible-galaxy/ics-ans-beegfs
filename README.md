ics-ans-beegeefs
===================

Ansible playbook to install beegeefs.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
